echo "Configuration du proxy sur 10.16.59.254"
cat proxy >> /etc/environment
# MAJ des paquets et installation de logiciel
apt update && apt upgrade -y
apt install nginx git python3-pip openssh-server supervisord
# apt install geogebra chromium
# Configuration de pyhton
echo "Configuration des dépendances Python"
pip3 install numpy sympy scipy matplotlib
pip3 install folium
pip3 install flask flask-socketio
pip3 install pillow
pip3 install gunicorn
echo "Creation des repértoires"
mkdir ~/workspace
cd ~/workspace
git clone git@framagit.org:xtinoux/metrics_reseau.git
git clone git@framagit.org:xtinoux/exomorphisme.git
echo "Configuration de supervisor & nginx" 
mv /etc/supervisor/supervisord.conf /etc/supervisor/supervisord.conf.old
mv supervisord.conf /etc/supervisor/supervisord.conf