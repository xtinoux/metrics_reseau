import socket
base = "10.16"
plage_reseau = [ f"{base}.{str(i)}.{str(k)}" for i,j in zip([56,57,58,59],[range(50,255),range(1,255),range(1,255)]) for k in j ]


started_hosts = []
stoped_hosts = []

def _host_status(ip):
    socket.setdefaulttimeout(1)
    host = {"ip":ip}
    try:
        host["hostname"], host["alias"], host["addresslist"] = socket.gethostbyaddr(ip)
        started_hosts.append(host)
    except socket.herror:
        host["hostname"], host["alias"], host["addresslist"] = "","",""
        stoped_hosts.append(host)
    return host

def get_macadress(ip):
    """

    """
    import subprocess
    import re
    process = subprocess.run(["arp", "-a", ip], capture_output=True)
    _exit = process.stdout.decode("utf-8")
    print(_exit)
    mac = re.search(r"(([a-f\d]{1,2}\:){5}[a-f\d]{1,2})", _exit).groups()[0]
    return mac

def reseau_status():
    started_hosts = []
    stoped_hosts = []
    [ _host_status(ip) for ip in plage_reseau]

def is_start(hostname):
    return hostname in started_hosts

if __name__ == "__main__":
    reseau_status()
    for host in started_hosts:
        print(f"""L'adresse mac du poste est {get_macadress(host["ip"])}""")