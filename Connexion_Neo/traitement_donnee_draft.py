import csv 
import os
import datetime
import recup
from client_ftp import send_file
from jinja2 import Template,Environment, BaseLoader, FileSystemLoader

# S'assurer qu'on soit bien dans le dossier du script
BASEDIR = os.path.dirname(__file__)
os.chdir(BASEDIR)
datas = []
# Récupération du dernier CSV
# recup.make_csv()
# Créer un date time ( jj/mm/aaaa h:m:s) 
now = datetime.datetime.now()
date_time = now.strftime("%d/%m/%Y, %H:%M:%S")
date_name = now.strftime("%Y-%m-%d")

# 
def delta_derniere_connexion(eleve):
    """ 
    A partir d'un dictionnaire élève renvoie le delta de derniere connexion en seconde
    """
    current_date = datetime.datetime.now()
    tmp = eleve['Dernière connexion'].split(" ")
    try:
        jour,mois,annee = tmp[0].split('-')
        heure, minute = tmp[1].split(":")
        last_connexion = datetime.datetime(int(annee), int(mois), int(jour), int(heure), int(minute))
        return (current_date - last_connexion).total_seconds()
    except:
        print(tmp[0])
        return 0

# Calcul des differentes barres
b1 = 24*60*60
b2 = 3*24*60*60

# Lecture du CSV
datas = []
with open('connexion_neo.csv','r', encoding='utf-8') as f:
    reader = csv.DictReader(f, delimiter=";")
    for row in reader:
        datas.append(dict(row))


eleves = [data for data in datas if data["Type"] == "Élève"]
# Le bilan temporel 
timedatas = []
with open(os.path.join('data','data_connexion_neo.csv'),'r', encoding='utf-8') as f:
    reader = csv.DictReader(f, delimiter=";")
    for row in reader:
        timedatas.append(dict(row))


# Création des classes
classes = list(set( [ data['Classes'] for data in eleves ]))
print(classes)
secondes = [ classe for classe in classes if "20" in classe or "21" in classe ]
permieres_g = [ classe for classe in classes if "1G" in classe ]
permieres_t = [ classe for classe in classes if "1STMG" in classe ]
terminales_g = [ classe for classe in classes if "TG" in classe ]
terminales_t = [ classe for classe in classes if "TSTMG" in classe ]
bts = [ classe for classe in classes if classe in ["BTSPASS1", "MCO2", "MCO1", "NDRC2", "NDRC1"]]
non_pro = secondes + permieres_g + permieres_t + terminales_g + terminales_t + bts
print("Non pro")
print(non_pro)

pro = [ classe for classe in classes if not classe in non_pro ]
print("pro")
print(pro)
secondes.sort()
permieres_g.sort()
permieres_t.sort()
terminales_g.sort()
terminales_t.sort()
bts.sort()
pro.sort()


def make_classes(classes):
    data_classes = []
    for classe in classes:
        eleves_classe = [ eleve for eleve in eleves if eleve["Classes"] == classe]
        connexion_recente = [ eleve for eleve in eleves_classe if delta_derniere_connexion(eleve) <= b1]
        connexion_moyenne = [ eleve for eleve in eleves_classe if delta_derniere_connexion(eleve) > b1 and delta_derniere_connexion(eleve) <= b2 ]
        connexion_ancienne = [ eleve for eleve in eleves_classe if delta_derniere_connexion(eleve) > b2 ]
        dic_data = {"nom":classe, "eleves":eleves_classe, "nb_eleves":len(eleves_classe),
            "eleves_connexion_recente":connexion_recente, "nb_connnexion_recente":len(connexion_recente), "pourcentage_recente": round(len(connexion_recente)/len(eleves_classe)*100,2),
            "eleves_connexion_moyenne":connexion_moyenne, "nb_connnexion_moyenne":len(connexion_moyenne), "pourcentage_moyenne": round(len(connexion_moyenne)/len(eleves_classe)*100,2),
            "eleves_connexion_ancienne":connexion_ancienne, "nb_connnexion_ancienne":len(connexion_ancienne), "pourcentage_ancienne": round(len(connexion_ancienne)/len(eleves_classe)*100,2),
            }
        data_classes.append(dic_data)
    return data_classes


def make_data_niveau(niveau):
    datas = {"pourcentage_recente" : [classe["pourcentage_recente"] for classe in niveau ],
            "m_recente" : round(sum([classe["pourcentage_recente"] for classe in niveau ])/len(niveau),2),
            "pourcentage_moyenne" : [classe["pourcentage_moyenne"] for classe in niveau ],
            "m_moyenne" : round(sum([classe["pourcentage_moyenne"] for classe in niveau ])/len(niveau),2),
            "pourcentage_ancienne" : [classe["pourcentage_ancienne"] for classe in niveau ],
            "m_ancienne" : round(sum([classe["pourcentage_ancienne"] for classe in niveau ])/len(niveau),2),
            "classes":[classe["nom"] for classe in niveau ]}
    return datas 


classe_seconde = make_classes(secondes)
classes_premiere_g = make_classes(permieres_g)
classes_premiere_t = make_classes(permieres_t)
classes_term_g = make_classes(terminales_g)
classes_term_t = make_classes(terminales_t)
classes_bts = make_classes(bts)
classe_pro = make_classes(pro)
 
# 
#  Data par niveau du lycée
# 
data_secondes =  make_data_niveau(classe_seconde)
data_premieres_g =   make_data_niveau(classes_premiere_g) 
data_premieres_t =  make_data_niveau(classes_premiere_t)
data_terminales_g = make_data_niveau(classes_term_g)
data_terminales_t = make_data_niveau(classes_term_t)
data_bts = make_data_niveau(classes_bts)
data_pro = make_data_niveau(classe_pro)

# 
#  Data global lycée
# 
data_lycee = {"pourcentage_recente" : round(100*len([ eleve for eleve in eleves if delta_derniere_connexion(eleve) <= b1])/len(eleves),0),
            "pourcentage_moyenne" : round(100*len([ eleve for eleve in eleves if delta_derniere_connexion(eleve) > b1 and delta_derniere_connexion(eleve) <= b2 ])/len(eleves),0),
            "pourcentage_ancienne" : round(100*len([ eleve for eleve in eleves if delta_derniere_connexion(eleve) > b2])/len(eleves),0),
            "classes":"Lycée de Sada"}

#
# Stockage du bilan dans le csv
#
with open("data/data_connexion_neo.csv", "a") as csvfile:
    data_lycee["date"] = date_time
    fieldnames = data_lycee.keys()
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    writer.writerow(data_lycee)

COLORS = ['rgba(200,134,145,0.8)','rgba(173,133,186,0.8)','rgba(140,161,195,0.8)','rgba(116,161,142,0.8)','rgba(129,173,181,0.8)','rgba(178,200,145,0.8)','rgba(185,156,107,0.8)','rgba(228,153,105,0.8)','rgba(201,194,127,0.8)','rgba(148,148,148,0.8)']
 
# Le bilan temporel 
timedatas = []
with open(os.path.join('data','data_connexion_neo.csv'),'r', encoding='utf-8') as f:
    reader = csv.DictReader(f, delimiter=",")
    for row in reader:
        timedatas.append(dict(row))

datas_timedate = {}
datas_timedate["dates"] = [ elmt["date"] for elmt in timedatas ]
datas_timedate["datas"] = [
        { "label" : "Connexion de moins de 24h",
          "data" : [ elmt["pourcentage_recente"] for elmt in timedatas ],
          "couleur" : "rgba(135,200,145,0.8)" },
        { "label" : "Connexion entre 24h et 72h",
          "data" : [ elmt["pourcentage_moyenne"] for elmt in timedatas ],
          "couleur" : 'rgba(100,134,200,0.8)'}, 
        { "label" : "Connexion de plus 72h",
          "data" : [ elmt["pourcentage_ancienne"] for elmt in timedatas ],
          "couleur" : 'rgba(200,134,135,0.8)' }
        ]       


template_env = Environment(
    loader=FileSystemLoader('./templates/'))
with open("dashboard.html", "w") as f:
    my_tpl = template_env.get_template('tmp.html')
    dashboard = my_tpl.render(
        secondes = data_secondes,
        premieres = data_premieres_g,
        premieres_t = data_premieres_t,
        terminales = data_terminales_g,
        terminales_t = data_terminales_t,
        bts = data_bts,
        pro = data_pro,
        lycee = data_lycee,
        datas = datas_timedate,
        date_time = date_time)
    f.write(dashboard)


with open(f"dashboard_{date_name}.html", "w") as f:
    my_tpl = template_env.get_template('tmp.html')
    dashboard = my_tpl.render(
        secondes = data_secondes,
        premieres = data_premieres_g,
        premieres_t = data_premieres_t,
        terminales = data_terminales_g,
        terminales_t = data_terminales_t,
        bts = data_bts,
        pro = data_pro,
        lycee = data_lycee,
        datas = datas_timedate,
        date_time = date_time)
    f.write(dashboard)




# send_file("dashboard.html", "connexion_neo")