import csv 
import os
import datetime
import recup
from client_ftp import send_file
from jinja2 import Template,Environment, BaseLoader, FileSystemLoader




BASEDIR = os.path.dirname(__file__)
os.chdir(BASEDIR)
datas = []
now = datetime.datetime.now()
date_time = now.strftime("%m/%d/%Y, %H:%M:%S")
# Récupération des CSV ....
recup.make_csv()
# Lecture du fichier CSV
with open('connexion_neo.csv','r', encoding='utf-8') as f:
    reader = csv.DictReader(f, delimiter=";")
    for row in reader:
        datas.append(dict(row))


eleves = [data for data in datas if data["Type"] == "Élève"]

def delta_derniere_connexion(eleve):
    """ 
    A partir d'un dictionnaire élève renvoie le delta de derniere connexion en seconde
    """
    current_date = datetime.datetime.now()
    tmp = eleve['Dernière connexion'].split(" ")
    try:
        jour,mois,annee = tmp[0].split('-')
        heure, minute = tmp[1].split(":")
        last_connexion = datetime.datetime(int(annee), int(mois), int(jour), int(heure), int(minute))
        return (current_date - last_connexion).total_seconds()
    except:
        print(tmp[0])
        return 0

# Calcul des differentes barres
b1 = 24*60*60
b2 = 72*24*60*60

# Création des classes
classes = list(set( [ data['Classes'] for data in eleves ]))
# print(classes)
secondes = [ classe for classe in classes if "20" in classe or "21" in classe ]
permieres_g = [ classe for classe in classes if "1G" in classe ]
permieres_t = [ classe for classe in classes if "1STMG" in classe ]
terminales_g = [ classe for classe in classes if "TG" in classe ]
terminales_t = [ classe for classe in classes if "TSTMG" in classe ]

secondes.sort()
permieres_g.sort()
permieres_t.sort()
terminales_g.sort()
terminales_t.sort()

print(secondes)
print(permieres_g)
print(permieres_t)
print(terminales_g)
print(terminales_t)


classes_seconde = []
for seconde in secondes:
    eleves_secondes = [ eleve for eleve in eleves if eleve["Classes"] == seconde]
    connexion_recente = [ eleve for eleve in eleves_secondes if delta_derniere_connexion(eleve) <= b1]
    connexion_moyenne = [ eleve for eleve in eleves_secondes if delta_derniere_connexion(eleve) > b1 and delta_derniere_connexion(eleve) <= b2 ]
    connexion_ancienne = [ eleve for eleve in eleves_secondes if delta_derniere_connexion(eleve) > b2 ]
    dic_seconde = {"nom":seconde, "eleves":eleves_secondes, "nb_eleves":len(eleves_secondes),
        "eleves_connexion_recente":connexion_recente, "nb_connnexion_recente":len(connexion_recente), "pourcentage_recente": round(len(connexion_recente)/len(eleves_secondes)*100,2),
        "eleves_connexion_moyenne":connexion_moyenne, "nb_connnexion_moyenne":len(connexion_moyenne), "pourcentage_moyenne": round(len(connexion_moyenne)/len(eleves_secondes)*100,2),
        "eleves_connexion_ancienne":connexion_ancienne, "nb_connnexion_ancienne":len(connexion_ancienne), "pourcentage_ancienne": round(len(connexion_ancienne)/len(eleves_secondes)*100,2),
        }
    classes_seconde.append(dic_seconde)


classes_premiere = []
for permiere in permieres_g:
    eleves_premieres = [ eleve for eleve in eleves if eleve["Classes"] == permiere]
    connexion_recente = [  eleve for eleve in eleves_premieres if delta_derniere_connexion(eleve) <= b1]
    connexion_moyenne = [  eleve for eleve in eleves_premieres if delta_derniere_connexion(eleve) > b1 and delta_derniere_connexion(eleve) <= b2 ]
    connexion_ancienne = [  eleve for eleve in eleves_premieres if delta_derniere_connexion(eleve) > b2 ]
    dic_premiere = {"nom":permiere, "eleves":eleves_premieres, "nb_eleves":len(eleves_premieres),
        "eleves_connexion_recente":connexion_recente, "nb_connnexion_recente":len(connexion_recente), "pourcentage_recente": round(len(connexion_recente)/len(eleves_premieres)*100,2),
        "eleves_connexion_moyenne":connexion_moyenne, "nb_connnexion_moyenne":len(connexion_moyenne), "pourcentage_moyenne": round(len(connexion_moyenne)/len(eleves_premieres)*100,2),
        "eleves_connexion_ancienne":connexion_ancienne, "nb_connnexion_ancienne":len(connexion_ancienne), "pourcentage_ancienne": round(len(connexion_ancienne)/len(eleves_premieres)*100,2),
        }
    classes_premiere.append(dic_premiere)


classes_terminale = []
for terminale in terminales_g:
    eleves_terminale = [ eleve for eleve in eleves if eleve["Classes"] == terminale]
    connexion_recente = [ eleve for eleve in eleves_terminale if delta_derniere_connexion(eleve) <= b1]
    connexion_moyenne = [ eleve for eleve in eleves_terminale if delta_derniere_connexion(eleve) > b1 and delta_derniere_connexion(eleve) <= b2 ]
    connexion_ancienne = [ eleve for eleve in eleves_terminale if delta_derniere_connexion(eleve) > b2 ]
    dic_terminale = {"nom":terminale, "eleves":eleves_terminale, "nb_eleves":len(eleves_terminale),
        "eleves_connexion_recente":connexion_recente, "nb_connnexion_recente":len(connexion_recente), "pourcentage_recente": round(len(connexion_recente)/len(eleves_terminale)*100,2),
        "eleves_connexion_moyenne":connexion_moyenne, "nb_connnexion_moyenne":len(connexion_moyenne), "pourcentage_moyenne": round(len(connexion_moyenne)/len(eleves_terminale)*100,2),
        "eleves_connexion_ancienne":connexion_ancienne, "nb_connnexion_ancienne":len(connexion_ancienne), "pourcentage_ancienne": round(len(connexion_ancienne)/len(eleves_terminale)*100,2),
        }
    classes_terminale.append(dic_terminale)

classes_premiere = []
for permiere in permieres_g:
    eleves_premieres = [ eleve for eleve in eleves if eleve["Classes"] == permiere]
    connexion_recente = [  eleve for eleve in eleves_premieres if delta_derniere_connexion(eleve) <= b1]
    connexion_moyenne = [  eleve for eleve in eleves_premieres if delta_derniere_connexion(eleve) > b1 and delta_derniere_connexion(eleve) <= b2 ]
    connexion_ancienne = [  eleve for eleve in eleves_premieres if delta_derniere_connexion(eleve) > b2 ]
    dic_premiere = {"nom":permiere, "eleves":eleves_premieres, "nb_eleves":len(eleves_premieres),
        "eleves_connexion_recente":connexion_recente, "nb_connnexion_recente":len(connexion_recente), "pourcentage_recente": round(len(connexion_recente)/len(eleves_premieres)*100,2),
        "eleves_connexion_moyenne":connexion_moyenne, "nb_connnexion_moyenne":len(connexion_moyenne), "pourcentage_moyenne": round(len(connexion_moyenne)/len(eleves_premieres)*100,2),
        "eleves_connexion_ancienne":connexion_ancienne, "nb_connnexion_ancienne":len(connexion_ancienne), "pourcentage_ancienne": round(len(connexion_ancienne)/len(eleves_premieres)*100,2),
        }
    classes_premiere.append(dic_premiere)

classes_premiere_t = []
for permiere in permieres_t:
    eleves_premieres = [ eleve for eleve in eleves if eleve["Classes"] == permiere]
    connexion_recente = [  eleve for eleve in eleves_premieres if delta_derniere_connexion(eleve) <= b1]
    connexion_moyenne = [  eleve for eleve in eleves_premieres if delta_derniere_connexion(eleve) > b1 and delta_derniere_connexion(eleve) <= b2 ]
    connexion_ancienne = [  eleve for eleve in eleves_premieres if delta_derniere_connexion(eleve) > b2 ]
    dic_premiere = {"nom":permiere, "eleves":eleves_premieres, "nb_eleves":len(eleves_premieres),
        "eleves_connexion_recente":connexion_recente, "nb_connnexion_recente":len(connexion_recente), "pourcentage_recente": round(len(connexion_recente)/len(eleves_premieres)*100,2),
        "eleves_connexion_moyenne":connexion_moyenne, "nb_connnexion_moyenne":len(connexion_moyenne), "pourcentage_moyenne": round(len(connexion_moyenne)/len(eleves_premieres)*100,2),
        "eleves_connexion_ancienne":connexion_ancienne, "nb_connnexion_ancienne":len(connexion_ancienne), "pourcentage_ancienne": round(len(connexion_ancienne)/len(eleves_premieres)*100,2),
        }
    classes_premiere_t.append(dic_premiere)


classes_terminale = []
for terminale in terminales_g:
    eleves_terminale = [ eleve for eleve in eleves if eleve["Classes"] == terminale]
    connexion_recente = [ eleve for eleve in eleves_terminale if delta_derniere_connexion(eleve) <= b1]
    connexion_moyenne = [ eleve for eleve in eleves_terminale if delta_derniere_connexion(eleve) > b1 and delta_derniere_connexion(eleve) <= b2 ]
    connexion_ancienne = [ eleve for eleve in eleves_terminale if delta_derniere_connexion(eleve) > b2 ]
    dic_terminale = {"nom":terminale, "eleves":eleves_terminale, "nb_eleves":len(eleves_terminale),
        "eleves_connexion_recente":connexion_recente, "nb_connnexion_recente":len(connexion_recente), "pourcentage_recente": round(len(connexion_recente)/len(eleves_terminale)*100,2),
        "eleves_connexion_moyenne":connexion_moyenne, "nb_connnexion_moyenne":len(connexion_moyenne), "pourcentage_moyenne": round(len(connexion_moyenne)/len(eleves_terminale)*100,2),
        "eleves_connexion_ancienne":connexion_ancienne, "nb_connnexion_ancienne":len(connexion_ancienne), "pourcentage_ancienne": round(len(connexion_ancienne)/len(eleves_terminale)*100,2),
        }
    classes_terminale.append(dic_terminale)


classes_terminale_t = []
for terminale in terminales_t:
    eleves_terminale = [ eleve for eleve in eleves if eleve["Classes"] == terminale]
    connexion_recente = [ eleve for eleve in eleves_terminale if delta_derniere_connexion(eleve) <= b1]
    connexion_moyenne = [ eleve for eleve in eleves_terminale if delta_derniere_connexion(eleve) > b1 and delta_derniere_connexion(eleve) <= b2 ]
    connexion_ancienne = [ eleve for eleve in eleves_terminale if delta_derniere_connexion(eleve) > b2 ]
    dic_terminale = {"nom":terminale, "eleves":eleves_terminale, "nb_eleves":len(eleves_terminale),
        "eleves_connexion_recente":connexion_recente, "nb_connnexion_recente":len(connexion_recente), "pourcentage_recente": round(len(connexion_recente)/len(eleves_terminale)*100,2),
        "eleves_connexion_moyenne":connexion_moyenne, "nb_connnexion_moyenne":len(connexion_moyenne), "pourcentage_moyenne": round(len(connexion_moyenne)/len(eleves_terminale)*100,2),
        "eleves_connexion_ancienne":connexion_ancienne, "nb_connnexion_ancienne":len(connexion_ancienne), "pourcentage_ancienne": round(len(connexion_ancienne)/len(eleves_terminale)*100,2),
        }
    classes_terminale_t.append(dic_terminale)


# 
#  CLASSES du lycée
# 
secondes = {"pourcentage_recente" : [classe["pourcentage_recente"] for classe in classes_seconde ],
            "m_recente" : round(sum([classe["pourcentage_recente"] for classe in classes_seconde ])/len(classes_seconde),2),
            "pourcentage_moyenne" : [classe["pourcentage_moyenne"] for classe in classes_seconde ],
            "m_moyenne" : round(sum([classe["pourcentage_moyenne"] for classe in classes_seconde ])/len(classes_seconde),2),
            "pourcentage_ancienne" : [classe["pourcentage_ancienne"] for classe in classes_seconde ],
            "m_ancienne" : round(sum([classe["pourcentage_ancienne"] for classe in classes_seconde ])/len(classes_seconde),2),
            "classes":[classe["nom"] for classe in classes_seconde ]}



premieres = {"pourcentage_recente" : [classe["pourcentage_recente"] for classe in classes_premiere ],
            "m_recente" : round(sum([classe["pourcentage_recente"] for classe in classes_premiere ])/len(classes_premiere),2),
            "pourcentage_moyenne" : [classe["pourcentage_moyenne"] for classe in classes_premiere ],
            "m_moyenne" : round(sum([classe["pourcentage_moyenne"] for classe in classes_premiere ])/len(classes_premiere),2),
            "pourcentage_ancienne" : [classe["pourcentage_ancienne"] for classe in classes_premiere ],
            "m_ancienne" : round(sum([classe["pourcentage_ancienne"] for classe in classes_premiere ])/len(classes_premiere),2), 
            "classes":[classe["nom"] for classe in classes_premiere ]}


premieres_t = {"pourcentage_recente" : [classe["pourcentage_recente"] for classe in classes_premiere_t ],
            "m_recente" : round(sum([classe["pourcentage_recente"] for classe in classes_premiere_t ])/len(classes_premiere_t),2),
            "pourcentage_moyenne" : [classe["pourcentage_moyenne"] for classe in classes_premiere_t ],
            "m_moyenne" : round(sum([classe["pourcentage_moyenne"] for classe in classes_premiere_t ])/len(classes_premiere_t),2),
            "pourcentage_ancienne" : [classe["pourcentage_ancienne"] for classe in classes_premiere_t ],
            "m_ancienne" : round(sum([classe["pourcentage_ancienne"] for classe in classes_premiere ])/len(classes_premiere),2), 
            "classes":[classe["nom"] for classe in classes_premiere_t ]}


terminales = {"pourcentage_recente" : [classe["pourcentage_recente"] for classe in classes_terminale ],
            "m_recente" : round(sum([classe["pourcentage_recente"] for classe in classes_terminale ])/len(classes_terminale),2),
            "pourcentage_moyenne" : [classe["pourcentage_moyenne"] for classe in classes_terminale ],
            "m_moyenne" : round(sum([classe["pourcentage_moyenne"] for classe in classes_terminale ])/len(classes_terminale),2),
            "pourcentage_ancienne" : [classe["pourcentage_ancienne"] for classe in classes_terminale ],
            "m_ancienne" : round(sum([classe["pourcentage_ancienne"] for classe in classes_terminale ])/len(classes_terminale),2),
            "classes":[classe["nom"] for classe in classes_terminale ]}


terminales_t = {"pourcentage_recente" : [classe["pourcentage_recente"] for classe in classes_terminale_t ],
            "m_recente" : round(sum([classe["pourcentage_recente"] for classe in classes_terminale_t ])/len(classes_terminale_t),2),
            "pourcentage_moyenne" : [classe["pourcentage_moyenne"] for classe in classes_terminale_t ],
            "m_moyenne" : round(sum([classe["pourcentage_moyenne"] for classe in classes_terminale_t ])/len(classes_terminale_t),2),
            "pourcentage_ancienne" : [classe["pourcentage_ancienne"] for classe in classes_terminale_t ],
            "m_ancienne" : round(sum([classe["pourcentage_ancienne"] for classe in classes_terminale_t ])/len(classes_terminale_t),2),
            "classes":[classe["nom"] for classe in classes_terminale_t ]}


lycee = {"pourcentage_recente" : round(len([ eleve for eleve in eleves if delta_derniere_connexion(eleve) <= b1])/len(eleves),2),
            "pourcentage_moyenne" : round(len([ eleve for eleve in eleves if delta_derniere_connexion(eleve) > b1 and delta_derniere_connexion(eleve) <= b2 ])/len(eleves),2),
            "pourcentage_ancienne" : round(len([ eleve for eleve in eleves if delta_derniere_connexion(eleve) > b2])/len(eleves),2),
            "classes":"Lycée de Sada"}



print(secondes["pourcentage_recente"])
# Création du nouveau fichier html
template_env = Environment(
    loader=FileSystemLoader('./templates/'))
with open("dashboard.html", "w") as f:
    my_tpl = template_env.get_template('tmp.html')
    dashboard = my_tpl.render(
        secondes = secondes,
        premieres = premieres,
        premieres_t = premieres_t,
        terminales = terminales,
        terminales_t = terminales_t,
        lycee =lycee,
        date_time = date_time)
    f.write(dashboard)
    
with open("dashboard_{{date_time}}.html", "w") as f:
    my_tpl = template_env.get_template('tmp.html')
    dashboard = my_tpl.render(
        secondes = secondes,
        premieres = premieres,
        premieres_t = premieres_t,
        terminales = terminales,
        terminales_t = terminales_t,
        lycee =lycee,
        date_time = date_time)
    f.write(dashboard)

send_file("dashboard.html", "connexion_neo")