import pandas
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import plotly.express as px

# This dataframe has 244 lines, but 4 distinct values for `day`
df = pandas.read_csv('connexion_eleve.csv', error_bad_lines=False)
print(df)
print(type(df))
app = dash.Dash(__name__)


app.layout = html.Div([
    html.P("Classe:"),
    dcc.Dropdown(
        id='Classe', 
        value='Classe', 
        options=[{'value': x, 'label': x} 
                 for x in ['Classe', 'day', 'time', 'sex','size']],
        clearable=False
    ),
    html.P("Values:"),
    dcc.Dropdown(
        id='Identifiants', 
        value='total_bill', 
        options=[{'value': x, 'label': x} 
                 for x in ['total_bill', 'tip', 'size']],
        clearable=False
    ),
    dcc.Graph(id="pie-chart"),
])


@app.callback(
    Output("pie-chart", "figure"), 
    [Input("classe", "value"), 
     Input("values", "value")])
def generate_chart(names, values):
    fig = px.pie(df, values=values, names=names)
    return fig


if __name__ == "__main__":
    app.run_server(debug=True)
