import csv 
import os
import datetime
import recup
from client_ftp import send_file
from jinja2 import Template,Environment, BaseLoader, FileSystemLoader
import pprint
# S'assurer qu'on soit bien dans le dossier du script
BASEDIR = os.path.dirname(__file__)
os.chdir(BASEDIR)
datas = []
# Récupération du dernier CSV
# recup.make_csv()
# Créer un date time ( jj/mm/aaaa h:m:s) 
now = datetime.datetime.now()
date_time = now.strftime("%d/%m/%Y %H:%M:%S")
COLORS = ['rgba(200,134,145,0.8)','rgba(173,133,186,0.8)','rgba(140,161,195,0.8)','rgba(116,161,142,0.8)','rgba(129,173,181,0.8)','rgba(178,200,145,0.8)','rgba(185,156,107,0.8)','rgba(228,153,105,0.8)','rgba(201,194,127,0.8)','rgba(148,148,148,0.8)']
 
# Le bilan temporel 
timedatas = []
with open(os.path.join('data','data_connexion_neo.csv'),'r', encoding='utf-8') as f:
    reader = csv.DictReader(f, delimiter=",")
    for row in reader:
        timedatas.append(dict(row))

datas = {}
datas["dates"] = [ elmt["date"] for elmt in timedatas ]
datas["datas"] = [
        { "label" : "Connexion de moins de 24h",
          "data" : [ elmt["pourcentage_recente"] for elmt in timedatas ],
          "couleur" : COLORS[0] },
        { "label" : "Connexion entre 24h et 72h",
          "data" : [ elmt["pourcentage_moyenne"] for elmt in timedatas ],
          "couleur" : COLORS[1] }, 
        { "label" : "Connexion de plus 72h",
          "data" : [ elmt["pourcentage_ancienne"] for elmt in timedatas ],
          "couleur" : COLORS[2] }
        ]       


template_env = Environment(
    loader=FileSystemLoader('./templates/'))
with open("test.html", "w") as f:
    my_tpl = template_env.get_template('tmpl.html')
    dashboard = my_tpl.render(
        datas = datas)
    f.write(dashboard)