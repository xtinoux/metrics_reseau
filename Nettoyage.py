#!/usr/bin/env python3

import glob
import os
from pathlib import Path

# On se place dans le dossier du script
dossier = os.path.dirname(os.path.abspath(__file__))
os.chdir(dossier)


def supr():
    for ext in ['.aux','.out','.synctex.gz','.fls','fdb_latexmk','.pyftpsync-meta.json','.directory','.DS_Store','.xdv']:

        try:
            for p in  Path('.').glob('./**/*{}'.format(ext)):
                os.remove('{}'.format(p))
        except:
            print("Pas de fichier {} a suprimmer".format(ext))


if __name__ == '__main__':
	supr()