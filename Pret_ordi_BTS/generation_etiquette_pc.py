import os 
import sys
import random
import numpy
from jinja2 import Template,Environment, BaseLoader, FileSystemLoader
# from flask import Flask,request,render_template
# On se place dans le dossier du script
dossier = os.path.dirname(os.path.abspath(__file__))
os.chdir(dossier)
"""from barcode import Code128 
from barcode.writer import ImageWriter"""
pcs = []
"""5,7,8, 17"""

# Créer une liste ............
for i in range(1,19):
    pc = {"codebarre":"CM1-PC-{}.png".format(str(i).zfill(2)), "num":str(i).zfill(2)}
    pcs.append(pc)
#    with open("CM1-PC-{}.png".format(str(i).zfill(2)), 'wb') as f:
#        Code128("CM1-PC-{}".format(str(i).zfill(2)), writer=ImageWriter()).write(f)
    #ean = barcode.get('code39', pc_name, writer=ImageWriter())
    #filename = ean.save('portable_1')

# make_template ...............
###############################
########### Template ##########
###############################
template_env = Environment(
	loader=FileSystemLoader('./templates/'),
	block_start_string="<!",
	block_end_string="!>",
	variable_start_string="<<",
	variable_end_string=">>",
	comment_start_string="<#",
	comment_end_string="#>")


tpl = "template"
with open(f"etiquette_cm1.tex", "w") as f:
    try:
        my_tpl = template_env.get_template('template.tex')
        enonce = my_tpl.render(
        pcs = pcs)
        f.write(enonce)
    except ZeroDivisionError:
        pass
    except ValueError:
        pass
"""
os.system('latexmk -cd -f -xelatex -interaction=nonstopmode -synctex=1 -latexoption=--shell-escape "etiquette_cm1.tex"')
os.system("rm  *.fls *.aux *.fdb_latexmk *.synctex.gz *.xdv *.out *.log")"""