import requests
import os
import hashlib

proxy = {
    "https":"10.16.59.254:3128",
    "http":"10.16.59.254:3128",
    "ftp":"10.16.59.254:3128",
}

liste_eleves ={
    "pass":"adminvr976",
    "submit":"liste_eleves.pdf",
    "filename":"liste_eleves.pdf",
    "content":"pdf"
    }

liste_profs ={
    "pass":"adminvr976",
    "submit":"Liste+des+profs",
    "filename":"liste_profs.pdf",
    "content":"pdf"
    }

code_eleves = {
    "pass":"adminvr976",
    "submit":"export_eleves.txt",
    "filename":"code_eleves.csv",
    "content":"text"
    }

code_profs = {
    "pass":"adminvr976",
    "submit":"export_profs.txt",
    "filename":"code_profs.csv",
    "content":"text"
    }

datas = [liste_eleves, liste_profs, code_eleves, code_profs]


url_login = "https://lpo-sada.ac-mayotte.fr"
url_document = "https://extranet.ac-mayotte.fr/appraip/telecharger.php?chemin=etabs/LycSada"


def get_code(data):
    reponse = requests.post(url_document, data=data)
    if data["content"] == "text":
        with open(data["filename"],"w") as f:
            print(reponse.text)
            f.write(reponse.text)
    else:
        with open(data["filename"],"wb") as f:
            print(reponse.content)
            f.write(reponse.content)


def go_to_temp():
    for data  in datas:
        src = data["filename"]
        dst = f"tmp_{data['filename']}"
        os.rename(src, dst)

def get_md5(fname):
    hash_md5 = hashlib.md5()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()

def is_new():
    for data in datas:
        src = data["filename"]
        dst = f"tmp_{data['filename']}"
        md5_src = get_md5(src)
        md5_tmp = get_md5(src)
        print(md5_src)
        print(md5_tmp)
        return not md5_src == md5_tmp

if __name__ == "__main__":
    print(liste_profs)
    get_code(liste_profs)