import qrcode
import os 
import sys
import time

from jinja2 import Template,Environment, BaseLoader, FileSystemLoader


def sup_temp():
    os.system(f"rm *etiquettes.pdf")
    os.system(f"rm *etiquettes.log")
    os.system(f"rm *etiquettes.out")
    os.system(f"rm *etiquettes.tex")
    os.system(f"rm *etiquettes.aux")

def make_etiquettes(pcs, salle):
    """
    Générer un fichier laTeX contenant 50 exercices
    """

    # from Nombre import rendu_nombre as rn
    template_env = Environment(
        loader=FileSystemLoader('./templates/'),
        block_start_string="<!",
        block_end_string="!>",
        variable_start_string="<<",
        variable_end_string=">>",
        comment_start_string="<#",
        comment_end_string="#>")
    with open(f"{salle}_tmp.tex", "w") as f:
        try:
            my_tpl = template_env.get_template(f'tpl_etiquettes.tex')
            enonce = my_tpl.render(pcs=pcs, salle=salle)
            f.write(enonce)
        except ZeroDivisionError:
            pass
        except ValueError:
            pass
            # enonce, correction = rendu_html(data)

salles = ["A001",
        "A002",
        "A003",
        "A004",
        "B005",
        "B006",
        "B007",
        "A101",
        "A102",
        "A103",
        "A104",
        "A105",
        "A106",
        "A106",
        "A100"]

salles = ["A206"]
postes = ['PP',"01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18"]
# postes.extend(list(range(1,19)))
for salle in salles:
    pcs = []
    for num_poste in postes:
        pc = {}
        pc["salle"] = salle
        pc["num"] = num_poste
        pc["nom"] = f"{salle}-{num_poste}"
        img = qrcode.make(f'mailto:tice-9760182u@ac-mayotte.fr?subject=Signalement sur le poste {pc["nom"]}&body=Description du dysfonctionnement du poste {pc["nom"]} de la salle {salle} : \n')
        img.save(f'qrcode-{pc["nom"]}.png')
        pcs.append(pc)
    make_etiquettes(pcs, salle)
    os.system(f"xelatex --interaction=nonstopmode {salle}_tmp.tex")
    time.sleep(2)
    os.system(f"pdftk {salle}_tmp.pdf cat 2 output {salle}.pdf")
    time.sleep(2)
    os.system(f"rm {salle}_tmp.tex")
    os.system(f"rm {salle}_tmp.aux")
    os.system(f"rm {salle}_tmp.log")
    os.system(f"rm {salle}_tmp.out")
    os.system(f"rm {salle}_tmp.pdf")
