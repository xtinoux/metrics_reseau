Write-Host '#############################'
Write-Host 'Creation des logiciels locaux'
Write-Host '#############################' 
Write-Host "--------------------------"
Write-Host "--------------------------"
Write-Host " Installation de :        "
Write-Host "      - Office 2003       "
Write-Host "      - Emulateur TI      "
Write-Host "      - MindView          "
Write-Host "      - Offscan           "
Write-Host "--------------------------"
Write-Host "--------------------------"
Write-Host "--------------------------"
Write-Host "Connexion au DC (srv182u) "
Write-Host "--------------------------"
net use \\10.16.56.1\paclog$\chocolatey /user:EDU\opc cpo
Write-Host "--------------------------"
Write-Host "Installation d'Office 2003"
Write-Host "--------------------------"
start notepad '\\10.16.56.1\paclog$\chocolatey\_install_etab\Office 2003\Cle Office 2003.txt'
& "\\10.16.56.1\paclog$\chocolatey\_install_etab\Office 2003\SETUP.EXE"
pause
& "\\10.16.56.1\paclog$\chocolatey\_install_etab\Convertisseur office 2007-2003\FileFormatConverters.exe"
pause
Write-Host "--------------------------"
Write-Host "Installation de l'émulateur TI"
Write-Host "--------------------------"
& "\\10.16.56.1\paclog$\chocolatey\_install_etab\Ti\TI-Connect_FR-4.0.0.218.exe"
& "\\10.16.56.1\paclog$\chocolatey\_install_etab\Ti\TI-SmartViewTI-83Plus.fr-1.1.0.exe"
pause
Write-Host "--------------------------"
Write-Host "Installation de MindView"
Write-Host "--------------------------"
start notepad '\\10.16.56.1\paclog$\chocolatey\_install_etab\Mindview\Mindview.txt'
& "\\10.16.56.1\paclog$\chocolatey\_install_etab\Mindview\mindview7_fr_b18668.exe"
pause
Write-Host "--------------------------"
Write-Host "Connexion a Winappli "
Write-Host "--------------------------"
net use \\10.16.56.2\ofcscan /user:EDU\opc cpo
Write-Host "--------------------------"
Write-Host "Installation de l'antivirus"
Write-Host "--------------------------"
& "\\10.16.56.2\ofcscan\autopcc.exe"
pause
Write-Host "--------------------------"
Write-Host "Installation de Filius"
Write-Host "--------------------------"
& "\\10.16.56.1\paclog$\chocolatey\_install_etab\filius\Filius-Setup-1.10.4.exe"
pause
Write-Host "--------------------------"
Write-Host "Installation de EDU Python"
Write-Host "--------------------------"
& "\\10.16.56.1\paclog$\chocolatey\_install_etab\eduPython\Setup-EP30.exe"
pause
Write-Host "--------------------------"
Write-Host "Installation de Iperf as Service"
Write-Host "--------------------------"
& "\\10.16.56.1\paclog$\chocolatey\_install_etab\Iperf\iperf_as_service.bat"
Write-Host "--------------------------"
Write-Host "Fin des installations "
Write-Host "--------------------------"
pause
Write-Host "--------------------------"
Write-Host "Installation de Fog"
Write-Host "--------------------------"
& "\\10.16.56.1\paclog$\chocolatey\_install_etab\fog\cmd.bat"
Write-Host "Le PC va redemarrer"
pause