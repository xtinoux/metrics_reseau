Write-Host '#####################################'
Write-Host 'Creation des logiciels par Chocolatey'
Write-Host '#####################################' 
Write-Host "--------------------------"
Write-Host "Connexion au DC (srv182u) "
Write-Host "--------------------------"
net use \\10.16.56.1\paclog$\chocolatey /user:EDU\opc cpo
Write-Host "--------------------------"
# Special pour Java 
mkdir C:\chocolatey
choco config set cacheLocation C:\chocolatey
choco install jre8 -y
# Special changement de dossier cache  pour les autres logiciels
choco config set cacheLocation \\10.16.56.6\chocolatey$
# Third party & Arduino /!\ Le drivers USB Necessite de rester devant le PC#
choco install dotnetfx -y
# Base #
choco install arduino -y # /!\
choco install adobereader -y
choco install Firefox --params "/NoTaskbarShortcut /NoAutoUpdate /NoMaintenanceService /l:fr-FR" -y
choco install fusioninventory-agent --installargs "/server='http://clet1426.odns.fr/glpi/plugins/fusioninventory/' /no-start-menu /runnow /proxy='http://10.16.59.254:3128'" -y
choco install 7zip -y
choco install vlc -y
choco install audacity -y
choco install libreoffice-still -y
choco install putty -y
choco install gimp -y
choco install audacity -y
choco install veyon -y
choco install ganttproject -y
choco install googleearth -y
choco install qgis -y
Write-Host  "Fin des installations de base"
Write-Host "Installations de Phy"           
choco install avogadro -y
Write-Host "Installations de SNT / NSI / Maths"
choco install sublimetext3 -y
choco install python3 -y
choco install anaconda3 -y
choco install sqlitebrowser -y
choco install sublimetext3 -y
choco install vscodium -y
choco install pyzo -y
choco install geogebra -y
choco install ti-connect-ce -y
Write-Host "Fin des installations de SNT / NSI / Maths"