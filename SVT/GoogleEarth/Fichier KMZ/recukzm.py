import requests
import shutil
from bs4 import BeautifulSoup
import os


url = "http://eduterre.ens-lyon.fr/outils/catalogue-deduterre/kmz"
r = requests.get(url)
html = r.text


soup =  BeautifulSoup(html)
for link in soup.find_all("a",href=True):
    if ".kmz" in link["href"]:
        print(f"Récupération du fichier {link}")
        path = link["href"].split("/")[-1].replace("%20","_")
        path = path.replace("__","_")
        if not path in os.listdir():
            if not 'http' in link["href"]:
                link["href"] = "http://eduterre.ens-lyon.fr" + link["href"]
            try:
                r = requests.get(link["href"], stream = True)
            except:
                print(f"Impossible de télécharger le lien {link}")
            if r.status_code == 200:
                with open(path, 'wb') as f:
                    r.raw.decode_content = True
                    shutil.copyfileobj(r.raw, f)  
        else:
            print("Le fichier existe déjà")