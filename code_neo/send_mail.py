import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import ssl
import csv
import os
import sys
# On se place dans le dossier du script
dossier = os.path.dirname(os.path.abspath(__file__))
os.chdir(dossier)



def to_plaintext(html):
    balises = [ {"<div>":""},
                {"</div>":""},
                {"</span>":""},
                {"<span>":""},
                {"<html>":""},
                {"</html>":""},
                {"<head>":""},
                {"</head>":""},
                {"<body>":""},
                {"</body>":""},
                {"<br>":""}
                ]
    for b in balises:
        for k,v in b.items():
            html = html.replace(k,v)
    return html


def login(email_address, email_pass, server):
    server.ehlo()
    context=ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
    context.set_ciphers('DEFAULT@SECLEVEL=1')
    server.starttls(context=context)
    server.ehlo()
    # Authentication
    server.login(email_address, email_pass)
    print("login")


def send_mail(destinataire, content, expediteur = "tice-976182u@ac-mayotte.fr"):
    # Create message container - the correct MIME type is multipart/alternative.
    msg = MIMEMultipart('alternative')
    msg['Subject'] = "Code Néo"
    msg['From'] = expediteur
    msg['To'] = destinataire
    # Create the body of the message (a plain-text and an HTML version).
    # Record the MIME types of both parts - text/plain and text/html.
    part1 = MIMEText(content, 'plain')
    # Attach parts into message container.
    # According to RFC 2046, the last part of a multipart message, in this case
    # the HTML message, is best and preferred.
    msg.attach(part1)
    # msg.attach(part2)

    # Send the message via local SMTP server.
    server = smtplib.SMTP("smtp.ac-mayotte.fr", 587)
    # email_address, email_pass = get_credentials("./credentials.txt")
    login("eclave", "m2ppsA$182u%", server)
    # sendmail function takes 3 arguments: sender's address, recipient's address
    # and message to send - here it is sent as one string.
    server.sendmail(expediteur, destinataire, msg.as_string())
    server.quit()


def get_content():
    contents = []
    fichier = "code_eleve_non_active.csv"
    with open(fichier, "r") as f:
        datas = [ dict(r) for r in csv.DictReader(f)]

    classes = list(set([ d["classe"] for d in datas]))
    print(datas)

    for classe in classes:
        eleves = [ d for d in datas if d["classe"] == classe ]
        if len(eleves) == 1:
            content = f"""Bonjour,   

    Il semblerait que {eleves[0]["Nom"]} {eleves[0]["Prenom"]}  de la classe {classe} n'a pas activé son compte Néo : <br>
    <ul>"""
        else:
            content = f"""Bonjour,  

Certains élèves de la classe de {classe} n'ont pas du activé leur compte Néo.
Ci dessous les identifiants :

"""
        for eleve in eleves:
            content += f"""
Identifiant : {eleve["identifiant"]} 
Mot de passe : {eleve["mdp"]} 

    """
        content +=""" 
        
Cordialement,
L'équipe numerique du LPO de Sada"""  
        contents.append({"content":content,"classe":classe,"mail":eleves[0]["mail"] })
    return contents


if __name__ == "__main__":
    contents = get_content()
    send_mail("xtinoux@gmail.com", contents[0]["content"]) 