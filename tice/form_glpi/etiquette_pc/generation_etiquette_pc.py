import os 
import sys
import random
import numpy
from jinja2 import Template,Environment, BaseLoader, FileSystemLoader
from salles import salles
# from flask import Flask,request,render_template
# On se place dans le dossier du script
dossier = os.path.dirname(os.path.abspath(__file__))
os.chdir(dossier)
salles = [salle for salle in salles if salle["nb_postes"] == 16] + [salle for salle in sorted(salles, reverse=True, key=lambda x : x["nb_postes"]) if salle["nb_postes"] != 16]
print(salles)
 

pcs = []
for salle in salles:
    pcs.append({"salle":salle["nom"],"pc_num":f"{salle['nom']}-PP"})
    for i in range(1,salle["nb_postes"]):
        pcs.append({"salle":salle["nom"],"pc_num":f"{salle['nom']}-{str(i).zfill(2)}"})

print(pcs)

 

# make_template ...............
###############################
########### Template ##########
###############################
template_env = Environment(
	loader=FileSystemLoader('./templates/'),
	block_start_string="<!",
	block_end_string="!>",
	variable_start_string="<<",
	variable_end_string=">>",
	comment_start_string="<#",
	comment_end_string="#>")


tpl = "template"
with open(f"etiquette_pc.tex", "w") as f:
    try:
        my_tpl = template_env.get_template(f'{tpl}.tex')
        enonce = my_tpl.render(
        pcs = pcs)
        f.write(enonce)
    except ZeroDivisionError:
        pass
    except ValueError:
        pass

os.system('latexmk -cd -f -xelatex -interaction=nonstopmode -synctex=1 -latexoption=--shell-escape "etiquette_pc.tex"')
os.system("rm  *.fls *.aux *.fdb_latexmk *.synctex.gz *.xdv *.out *.log")