import os
import sys
from flask import Flask, render_template, request, redirect
import requests
import json 
import os
from glpi_api import GLPI

sys.path.insert(0, os.path.dirname(__file__))

app = Flask(__name__, template_folder='templates', static_folder='static')

def close(glpi):
    glpi.close()
    return redirect('/api-glpi/mission_accomplie')


@app.route("/")
def index():
    return "api ticket"


@app.route('/send_ticket/', methods=['GET', 'POST'])
@app.route('/send_ticket/<salle>/', methods=['GET', 'POST'])
@app.route('/send_ticket/<salle>/<poste>', methods=['GET', 'POST'])
def send_ticket(salle=None, poste=None):
    glpi = GLPI()
    if request.method == "POST":
        res = request.form
        for r in res:
            print(r)
        args = {
            "secret":"6LdoZegZAAAAAOkwnPIQcJpjWmEfUvrSZJx_UsdD",
            "reponse":"resé"
        }
        titre = "Salle : {} - Poste : {}".format(res["salle"], res["poste"])
        description = res["description"]
        r = glpi.send_ticket(titre, description)
        print(r)
        if r == 201:
            return close(glpi)
        else:
            return "Oupsss ..."
    return render_template('send_ticket.html', salle=salle, poste=poste )


@app.route('/mission_accomplie/')
def mission_accomplie():
    return render_template('mission_accomplie.html')
############################
#      Page d'erreur      #
############################
@app.errorhandler(404)
def page_not_found(e):
    """
    Gestion de la page 404
    """
    return render_template('erreurs/404.html'), 404

@app.errorhandler(500)
def page_not_found(e):
    """
    Gestion de la page 500
    """
    return render_template('erreurs/500.html'), 500

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8910, debug=True)