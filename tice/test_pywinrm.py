import winrm


host = """ 
    "B211-01", 
    "B211-02", 
    "B211-03", 
    "B211-04", 
    "B211-05", 
    "B211-06", 
    "B211-07", 
    "B211-08", 
    "B211-09", 
    "B211-10", 
    "B211-11", 
    "B211-12", 
    "B211-13", 
    "B211-14", 
    "B211-15", 
    "B211-16", 
    "B211-17", 
    "B211-PP",
"""

for host in hosts:
    s = winrm.Session(f"{host}.edu.local", auth=("administrateur", "uf218m976"))
    r = s.run_cmd('ipconfig', ['/all'])
    print(r.status_code)