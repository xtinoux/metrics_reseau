import iperf3
import time
import datetime
import csv
import os 
from pprint import pprint
from datetime import datetime


BASEDIR = os.path.dirname(__file__)
os.chdir(BASEDIR)

class Client:
    def __init__(self, host, port = 5201 ):
        self.hostname = host["name"]
        self.client = iperf3.Client()
        self.client.server_ip = host["ip"]
        self.client.port = 5201
        self.client.duration = 1
        self.res = {"host":self.hostname, "date":None, "download":None, "upload":None, "latence":None}
    
    def make_metrics(self, nb_tests=1, gen_csv=True):
        sum_download = 0 
        sum_upload = 0 
        for i in range(nb_tests):
            result = self.client.run()
            if result.error:
                nb_tests -= 1
                return result.error
            datas = result.json
            sum_download += datas["end"]["sum_received"]["bits_per_second"]
            sum_upload +=  datas["end"]["sum_sent"]["bits_per_second"]
            if nb_tests > 1:
                time.sleep(30)
        self.res["download"] = int(sum_download / (nb_tests*1000*1000)) #resultat en Mbs
        self.res["upload"] = int(sum_upload / (nb_tests*1000*1000)) # resultat en Mbs
        self.res["latence"] = datas["end"]["streams"][0]["sender"]["mean_rtt"]
        now = datetime.now()
        dt_string = now.strftime("%Y-%m-%d %H:%M")
        self.res["date"] = dt_string
        pprint(self.res)
        if gen_csv:
            print("Génération du CSV")
            self.make_csv()
        return self.res

    def make_csv(self, csvfilepath="lan_datas.csv"):
        if not os.path.isfile(csvfilepath):
            print("Création du fichier csv")
            with open(csvfilepath,"w") as csvfile:
                writer = csv.writer(csvfile)
                writer.writerow(["host","date", "download", "upload", "latence"])
        with open(csvfilepath,"a") as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow([self.hostname, self.res["date"], self.res["download"], self.res["upload"], self.res["latence"] ])
    


if __name__ == "__main__":  
    host = get_host_by_ip("10.16.56.58")  
    iperf  = Client(host)
    res = iperf.make_metrics()
    print(res)