import iperf3
import time
import datetime
import csv
import os 
from pprint import pprint
from datetime import datetime
from constantes import *
from wake_up import wake_up
import hosts


BASEDIR = os.path.dirname(__file__)
os.chdir(BASEDIR)


class Client:
    def __init__(self, host, port = 5201 ):
        self.hostname = host["name"]
        self.client = iperf3.Client()
        if host["ip"]:
            self.client.server_hostname = host["ip"]
        else:
            self.client.server_hostname = host["name"]
        self.client.port = 5201
        self.client.duration = 30
        self.res = {"host":self.hostname, "date":None, "download":'', "upload":'', "latence":''}
    
    def make_metrics(self,now = None, gen_csv=True):
        result = self.client.run()
        if not result.error:
            datas = result.json
            download = datas["end"]["sum_received"]["bits_per_second"]
            upload = datas["end"]["sum_sent"]["bits_per_second"]
            self.res["download"] = int(download / (1000*1000)) #resultat en Mbs
            self.res["upload"] = int(upload / (1000*1000)) # resultat en Mbs
            self.res["latence"] = datas["end"]["streams"][0]["sender"]["mean_rtt"]
        if not now:
            now = datetime.now()
        dt_string = now.strftime("%Y-%m-%d %H:%M")
        self.res["date"] = dt_string
        if gen_csv:
            print("Génération du CSV")
            self.make_csv()
        return self.res

    def make_csv(self, csvfilepath="lan_datas.csv"):
        if not os.path.isfile(csvfilepath):
            print("Création du fichier csv")
            with open(csvfilepath,"w") as csvfile:
                writer = csv.writer(csvfile)
                writer.writerow(["host","date", "download", "upload", "latence"])
        with open(csvfilepath,"a") as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow([self.hostname, self.res["date"], self.res["download"], self.res["upload"], self.res["latence"] ])
    
    


if __name__ == "__main__":  
    host = hosts.get_host_by_name("SDP-08") 
    # print(help(iperf3.Client()))
    iperf  = Client(host)
    res = iperf.make_metrics()
    del iperf
    host = hosts.get_host_by_name("S215-LENOVO") 
    print(host)
    iperf  = Client(host)
    res = iperf.make_metrics()
    if res == "unable to connect to server: No route to host":
        wake_up(host)
        time.sleep(10)
        res = iperf.make_metrics()
    print(res)
