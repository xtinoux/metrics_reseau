from jinja2 import Template,Environment, BaseLoader, FileSystemLoader
from client_iperf import Client
from client_ftp import send_file, send_files
import os 
from pprint import pprint
import csv 
import random 
import constantes
import hosts
import time 

BASEDIR = os.path.dirname(__file__)
os.chdir(BASEDIR)
names = ["S215-LENOVO","A200-PP","SDP-08","F01-PP","CDI-INFO-PP","A204-PP","A202-PP","A205-PP","B211-PP","A206-PP"]
HOSTS = [hosts.get_host_by_name(name) for name in names]
pprint(HOSTS)


def test_host():
    for host in HOSTS:
        time.sleep(5)
        print(host)
        try:            
            iperf_cli = Client(host)
            iperf_cli.make_metrics()
            del iperf_cli
            # iperf_cli.make_csv()
        except Exception as e:
            print(e)



def make_html(htmlfilepath="lan_dashboard.html"):
    # Recuperation des données du csv
    datas = [] # {host:"",download:[], upload:[], ping:[],date :[]}
    dates = []
    with open("lan_datas.csv","r") as csvfile:
        reader = csv.DictReader(csvfile)
        keys = reader.fieldnames
        for row in reader:
            if not row["host"] in [data["host"] for data in datas]:
                datas.append({"host":row["host"],"download":[row["download"]], "upload":[row["upload"]], "latence":[row["latence"]],"date" :[row["date"]],"color":random.choice(constantes.COLORS)})
            else:
                host = [ data for data in datas if data["host"] == row["host"]][0]
                host["download"].append(row["download"])
                host["date"].append(row["date"])
                host["upload"].append(row["upload"])
                host["latence"].append(row["latence"])
                
        dates = datas[0]["date"]

    # Création du nouveau fichier html
    template_env = Environment(
        loader=FileSystemLoader('./templates/'))
    with open(htmlfilepath, "w") as f:
        my_tpl = template_env.get_template('tmpl.html')
        dashboard = my_tpl.render(
            datas = datas,
            dates = dates)
        f.write(dashboard)
    #envoi des fichies vers le serveur
    # send_files(["lan_datas.csv","lan_dashboard.html"], "metrics_lan")


if __name__ == "__main__":
    test_host()
    make_html()