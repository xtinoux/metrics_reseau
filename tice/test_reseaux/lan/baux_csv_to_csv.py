import csv
import os 

BASEDIR = os.path.dirname(__file__)
os.chdir(BASEDIR)

with open("baux_dhcp.csv","r") as f:
    datas = [ dict(row) for row in csv.DictReader(f,delimiter=",")]
with open("baux.csv","w") as f:
    fieldnames = ["name","mac","ip"]
    writer = csv.DictWriter(f, fieldnames=fieldnames)
    writer.writeheader()
    for row in datas:
        writer.writerow({"name": row["Nom"] , "mac": row["ID unique"] , "ip": row["Adresse IP du client"]})