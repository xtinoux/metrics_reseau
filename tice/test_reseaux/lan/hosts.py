import csv
import os 

BASEDIR = os.path.dirname(__file__)
os.chdir(BASEDIR)

############################################################
#
############################################################
def make_hosts(csvfile="hosts_export.csv"):
    with open(csvfile) as f:
        return [ dict(row)  for row in csv.DictReader(f) ]

############################################################
# GETTERS
############################################################
def get_host_by_name(name):
    try:
        return [ host for host in  HOSTS if host["name"] == name][0]
    except:
        return None  

def get_host_by_ip(ip):
    try:
        return [ host for host in  HOSTS if host["ip"] == ip][0]
    except:
        return None

def get_host_by_mac(name):
    try:
        return [ host for host in  HOSTS if host["mac"] == mac][0]
    except:
        return None

##############################################################################
##############################################################################
##############################################################################

HOSTS = make_hosts()
print(HOSTS)