from wakeonlan import send_magic_packet
from hosts import *

def wake_up(host):
    mac = host["mac"]
    mac.replace(":","-")
    send_magic_packet(mac)

if __name__ == "__main__":
    host = get_host_by_name('SDP-08')
    print(host)
    wake_up(host)