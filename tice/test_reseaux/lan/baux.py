# Import BeautifulSoup

from bs4 import BeautifulSoup as bs
content = []
# Read the XML file
with open("baux.xml", "r") as file:
    # Read each line in the file, readlines() returns a list of lines
    content = file.readlines()
    # Combine the lines in the list into a string
    content = "".join(content)
    bs_content = bs(content, "lxml")
#reservations =  bs_content.select("Reservation")
reservations =  bs_content.find_all("reservation")
print(len(reservations))