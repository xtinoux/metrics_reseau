from wakeonlan import send_magic_packet
from constantes import *

def wake_up(host):
    send_magic_packet(host["mac"])

def get_host_by_name(name):
    return [ host for host in  HOSTS if host["name"] == name][0]

def get_host_by_ip(ip):
    return [ host for host in  HOSTS if host["ip"] == ip][0]

def get_host_by_mac(name):
    return [ host for host in  HOSTS if host["mac"] == mac][0]

if __name__ == "__main__":
    host = get_host_by_ip('10.16.57.86')
    print(host)