import time
import datetime
import csv
import json
import os 
from pprint import pprint
from datetime import datetime
import subprocess
import platform    # For getting the operating system name

try:
    BASEDIR = os.path.dirname(__file__)
    os.chdir(BASEDIR)
except:
    pass


class Client:
    def __init__(self):
        self.res = {}

    def ping(self):
        """
        Returns True if host (str) responds to a ping request.
        Remember that a host may not respond to a ping (ICMP) request even if the host name is valid.
        """
        host = "google.fr"
        # Option for the number of packets as a function of
        param = '-n' if platform.system().lower()=='windows' else '-c'
        cmd_shell = "ping " + param + " 5 " + host
        # Building the command. Ex: "ping -c 1 google.com"
        cmd = subprocess.run(cmd_shell, shell=True, stdout=subprocess.PIPE)
        data = cmd.stdout.decode("utf-8")
        data = data.split("time")[-1].split("ms")
        return int(data[0])

    def make_metrics(self, gen_csv= True):
        # download 
        cmd_down = 'curl -4 --write-out %{speed_download} --silent  -o /dev/null http://bouygues.testdebit.info/100M.iso'
        cmd = subprocess.run(cmd_down, shell=True, stdout=subprocess.PIPE)
        self.res["download"] = float(cmd.stdout.decode("utf-8").replace(",","."))/(1000*1000)
        # upload
        cmd_up = 'curl -4 --write-out %{speed_upload} --silent  -o /dev/null -F "filecontent=tmp/temp.iso" http://bouygues.testdebit.info'
        cmd = subprocess.run(cmd_up, shell=True, stdout=subprocess.PIPE)
        self.res["upload"] = float(cmd.stdout.decode("utf-8").replace(",","."))/(1000*1000)
        self.res["latence"] = self.ping()
        now = datetime.now()
        dt_string = now.strftime("%Y-%m-%d %H:%M")
        self.res["date"] = dt_string
        if gen_csv:
            self.make_csv()
        return self.res

    def make_csv(self, csvfilepath="wan_datas.csv"):
        print("Mise à jour du CSV")
        if not os.path.isfile(csvfilepath):
            print("Création du fichier csv")
            with open(csvfilepath,"w") as csvfile:
                writer = csv.writer(csvfile)
                writer.writerow(["host","date", "download", "upload", "latence"])
        with open(csvfilepath,"a") as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow([self.res["date"], self.res["download"], self.res["upload"], self.res["latence"] ])
    

if __name__ == "__main__":
    cli_wan = Client()
    print(cli_wan.ping())
