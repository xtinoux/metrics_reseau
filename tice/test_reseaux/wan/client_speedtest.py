import time
import datetime
import csv
import json
import os 
from pprint import pprint
from datetime import datetime
import subprocess


BASEDIR = os.path.dirname(__file__)
os.chdir(BASEDIR)

class Client:
    def __init__(self):
        pass

    def make_metrics(self, nb_tests=1, gen_csv=True):
        sum_download = 0 
        sum_upload = 0 
        for i in range(nb_tests):
            cmd = subprocess.run("speedtest -f json",shell=True, capture_output=True)
            self.res = cmd.stdout.decode(encoding = 'utf-8')
            print(type(self.res))
            self.res = json.loads(self.res)
            sum_download += self.res["download"]["bandwidth"]
            sum_upload +=  self.res["upload"]["bandwidth"]
            if nb_tests > 1:
                time.sleep(300)
        self.res["download"] =round(sum_download / (nb_tests*125000),2) #division par 125000 pour resultat en Mbs
        self.res["upload"] = round(sum_upload / (nb_tests*125000), 2 ) # division par 125000  pour resultat en Mbs
        self.res["latence"] = int(self.res["ping"]["latency"])
        now = datetime.now()
        dt_string = now.strftime("%Y-%m-%d %H:%M")
        self.res["date"] = dt_string
        pprint(self.res)
        if gen_csv:
            print("Génération du CSV")
            self.make_csv()
        return self.res

    def make_csv(self, csvfilepath="wan_datas.csv"):
        if not os.path.isfile(csvfilepath):
            print("Création du fichier csv")
            with open(csvfilepath,"w") as csvfile:
                writer = csv.writer(csvfile)
                writer.writerow(["date", "download", "upload", "latence"])
        with open(csvfilepath,"a") as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow([ self.res["date"], self.res["download"], self.res["upload"], self.res["latence"] ])
    
if __name__ == "__main__":
    speed_cli = Client()
    print(speed_cli.make_metrics())