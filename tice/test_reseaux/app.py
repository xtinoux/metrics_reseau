from jinja2 import Template,Environment, BaseLoader, FileSystemLoader
from client_iperf import Client
from client_ftp import send_file, send_files
import os 
from pprint import pprint
import csv 

BASEDIR = os.path.dirname(__file__)
os.chdir(BASEDIR)

def make_html(htmlfilepath="lan_dashboard.html"):
    # Génréation de nouvelle metric
    iperf_cli = Client("192.168.1.28")
    iperf_cli.make_metrics()
    iperf_cli.make_csv()
    # Recuperation des données du csv
    datas = {}
    with open("lan_datas.csv","r") as csvfile:
        reader = csv.DictReader(csvfile)
        keys = reader.fieldnames
        for key in keys:
            datas[key] = []
        for row in reader:
            for key in keys:
                datas[key].append(row[key])
    # Création du nouveau fichier html
    template_env = Environment(
        loader=FileSystemLoader('./templates/'))
    with open(htmlfilepath, "w") as f:
        my_tpl = template_env.get_template('tmpl.html')
        dashboard = my_tpl.render(
            datas = datas)
        f.write(dashboard)
    #envoi des fichies vers le serveur
    send_files(["lan_datas.csv","lan_dashboard.html"], "debit_lan")

if __name__ == "__main__":
    make_html()