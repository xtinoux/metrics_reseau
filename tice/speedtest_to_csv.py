# import speedtest
import speedtest
import time 
import json
import datetime
import csv

import os 
from pprint import pprint
from datetime import datetime


BASEDIR = os.path.dirname(__file__)
os.chdir(BASEDIR)

class Client:
    def __init__(self, threads=1):
        self.threads = threads
        self.res = {}
        self.cli = speedtest.Speedtest()
        self.cli.get_best_server()
    
    def make_metrics(self, nb_tests=1, gen_csv=True):
        down, up, latence = 0, 0, 0
        for _ in range(nb_tests):
            self.cli.download(threads=self.threads)
            self.cli.upload(threads=self.threads)
            self.cli.results.share()
            self.tmp_res = self.cli.results.dict()
            down += round(self.tmp_res["download"]/(1000*1000), 3)
            up +=  round(self.tmp_res["upload"]/(1000*1000), 3)
            latence += self.tmp_res["ping"]
            if nb_tests > 1:
                time.sleep(120)
            # Calcul des moyennes
        self.res["date"] = datetime.now().strftime("%Y-%m-%d %H:%M")
        self.res["download"], self.res["upload"], self.res["latence"] = down/nb_tests, up/nb_tests, latence/nb_tests
        if  gen_csv:
            self.make_csv()
        return res

    def make_csv(self, csvfilepath="wan_datas.csv"):
        if not os.path.isfile(csvfilepath):
            print("Création du fichier csv")
            with open(csvfilepath,"w") as csvfile:
                writer = csv.writer(csvfile)
                writer.writerow(["date", "download", "upload", "latence"])
        with open(csvfilepath,"a") as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow([self.res["date"], self.res["download"], self.res["upload"], self.res["latence"] ])
    


if __name__ == "__main__":    
    speed_cli  = Client()
    res = speed_cli.make_metrics()
    print(res)
 

 