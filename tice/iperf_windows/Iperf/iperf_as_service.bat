::
:: Copie des fichier necessaire dans C:\iperf
::
mkdir C:\iperf
copy %~dp0\srvany.exe  C:\iperf\
copy %~dp0\cygwin1.dll C:\iperf\
copy %~dp0\instsrv.exe C:\iperf\
copy %~dp0\iperf3.exe  C:\iperf\
::
:: Install iperf3 as Windows service
::
SET iperfdir=C:\iperf
SET iperfprog=iperf3.exe
SET iperflog=iperf3-server-logs.txt
SET servicename=iperf3
SET start=auto
SET binpath=%iperfdir%\srvany.exe
SET iperfoptions=--server --daemon --port 5201 --version4 --format [m] --verbose --logfile %iperfdir%\%iperflog%
SET displayname=iPerf3 Service
SET description=iPerf3 Service provide a possibility to test network speed
::
::
sc.exe create %servicename% displayname= "%displayname%" start= %start% binpath= "%binpath%"
sc description %servicename% "%description%"
::
reg add HKLM\SYSTEM\CurrentControlSet\services\%servicename%\Parameters /v AppParameters /t REG_SZ /d "%iperfoptions%"
reg add HKLM\SYSTEM\CurrentControlSet\services\%servicename%\Parameters /v Application /t REG_SZ /d "%iperfdir%\%iperfprog%" /f
::
pause
::