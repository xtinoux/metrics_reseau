try
{
    $tsenv = New-Object -COMObject Microsoft.SMS.TSEnvironment
    $logPath = $tsenv.Value("LogPath")
}
catch
    {
    Write-Host "This script is not running in a task sequence"
    $logPath = $env:windir + "\temp"
}

$logFile = "$logPath\$($myInvocation.MyCommand).log"

# Start logging
Start-Transcript $logFile
Write-Host "Logging to $logFile"

# List of Applications that will be removed

$AppsList = "microsoft.windowscommunicationsapps","Microsoft.BingFinance","Microsoft.BingMaps",`
"Microsoft.BingWeather","Microsoft.ZuneVideo","Microsoft.ZuneMusic","Microsoft.Media.PlayReadyClient.2",`
"Microsoft.XboxLIVEGames","Microsoft.HelpAndTips","Microsoft.BingSports",`
"Microsoft.BingNews","Microsoft.BingFoodAndDrink","Microsoft.BingTravel","Microsoft.WindowsReadingList",`
"Microsoft.BingHealthAndFitness","Microsoft.WindowsAlarms","Microsoft.Reader","Microsoft.WindowsCalculator",`
"Microsoft.WindowsScan","Microsoft.WindowsSoundRecorder","Microsoft.SkypeApp","Microsoft.YourPhone"," Microsoft.Office.OneNote",`
"Microsoft.Photo","Microsoft.GetHelp","Microsoft3DViewer","MicrosoftOfficeHub","Microsoft.People","Microsoft.Print3D","Microsoft.WindowsAlarms",`
"Microsoft.WindowsMaps",`
"Microsoft.Xbox","Microsoft.XboxApp","Microsoft.XboxGameOverlay","Microsoft.XboxGamingOverlay","Microsoft.XboxIdentityProvider","Microsoft.XboxSpeechToTextOverlay"

ForEach ($App in $AppsList){
    $Packages = Get-AppxPackage | Where-Object {$_.Name -eq $App}
    if ($Packages -ne $null){
        Write-Host "Removing Appx Package: $App"
        foreach ($Package in $Packages){
            Remove-AppxPackage -AllUsers  -Package $Package.PackageFullName
        }
    }
    else{d
        Write-Host "Unable to find package: $App"
    }

$ProvisionedPackage = Get-AppxProvisionedPackage -online | Where-Object {$_.displayName -eq $App}
    if ($ProvisionedPackage -ne $null){
      Write-Host "Removing Appx Provisioned Package: $App"
      remove-AppxProvisionedPackage -online -packagename $ProvisionedPackage.PackageName
    }
    else{d
        Write-Host "Unable to find provisioned package: $App"
    }
}

# Stop logging
Stop-Transcript

