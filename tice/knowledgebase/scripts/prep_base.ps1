
echo "Création des comptes locaux"
net user eleve eleve /add
net user eleve  /Passwordchg:No
net user prof prof /add
net user prof  /Passwordchg:No
echo "activation l'administrateur"
net user Administrateur /Active:yes
net user Administrateur *
echo "Configuration du proxy"
netsh winhttp set proxy 10.16.59.254:3128
echo "Activation de WinRM"
Enable-PSRemoting -Force
winrm set winrm/config/service '@{AllowUnencrypted="true"}'
winrm set winrm/config/service/auth '@{Basic="true"}'
# Install the OpenSSH Client
Add-WindowsCapability -Online -Name OpenSSH.Client~~~~0.0.1.0
# Install the OpenSSH Server
Add-WindowsCapability -Online -Name OpenSSH.Server~~~~0.0.1.0
# Start the sshd service
Start-Service sshd
# OPTIONAL but recommended:
Set-Service -Name sshd -StartupType 'Automatic'
# Confirm the firewall rule is configured. It should be created automatically by setup.
Get-NetFirewallRule -Name *ssh*
# There should be a firewall rule named "OpenSSH-Server-In-TCP", which should be enabled
# If the firewall does not exist, create one
New-NetFirewallRule -Name sshd -DisplayName 'OpenSSH Server (sshd)' -Enabled T
echo "Installation de Chocolatey"
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
echo "Fin de la préparation de base"
echo "Vous pouvez lancer le script de desintallation des applications préinstallées ou installers les applications à l'aide de chocolatey"