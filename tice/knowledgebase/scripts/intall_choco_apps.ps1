netsh winhttp set proxy 10.16.59.254:3128
# Special pour Java 
mkdir C:\chocolatey
choco config set cacheLocation C:\chocolatey
choco install jre8 -y
# Special changement de dossier cache  pour les autres logiciels
choco config set cacheLocation \\10.16.56.6\chocolatey$
# Third party & Arduino /!\ Le drivers USB Necessite de rester devant le PC#
choco install arduino -y # /!\
choco install dotnetfx -y
choco install PowerShell -y
choco install adobereader -y
choco install wget -y
# Base #
choco install Firefox --params "/NoTaskbarShortcut /NoAutoUpdate /NoMaintenanceService /l:fr-FR" -y
choco install fusioninventory-agent --installargs "/server='http://clet1426.odns.fr/glpi/plugins/fusioninventory/' /no-start-menu /runnow /proxy='http://10.16.59.254:3128'" -y
choco install 7zip -y
choco install vlc -y
choco install audacity -y
choco install libreoffice-still -y
choco install putty -y
choco install gimp -y
choco install audacity -y
choco install veyon -y
choco install ganttproject -y
choco install googleearth -y
choco install qgis -y
echo "Fin des installations de base"

echo "Installations de SNT / NSI / Maths"
choco install sublimetext3 -y
choco install python3 -y
choco install anaconda3 -y
choco install sqlitebrowser -y
choco install sublimetext3 -y
choco install vscodium -y
choco install pyzo -y
choco install geogebra -y
choco install ti-connect-ce -y
echo "Fin des installations de SNT / NSI / Maths"


echo "Installations de Phy"           
choco install avogadro -y
#fin #


Get-ScheduledTask | Where-Object {$_.TaskName -match "Firefox"} | Unregister-ScheduledTask -Confirm:$false