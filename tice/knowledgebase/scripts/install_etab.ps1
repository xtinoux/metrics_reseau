
echo "--------------------------"
echo "--------------------------"
echo " Installation de :        "
echo "      - Office 2003       "
echo "      - Emulateur TI      "
echo "      - MindView          "
echo "      - Offscan           "
echo "--------------------------"
echo "--------------------------"

echo "--------------------------"
echo "Installation d'Office 2003"
echo "--------------------------"
start notepad '\\10.16.56.6\chocolatey$\_install_etab\Office 2003\Cle Office 2003.txt'
& "\\10.16.56.6\chocolatey$\_install_etab\Office 2003\SETUP.EXE"
& "\\10.16.56.6\chocolatey$\_install_etab\Convertisseur office 2007-2003\FileFormatConverters.exe"
pause
echo "--------------------------"
echo "Installation de l'émulateur TI"
echo "--------------------------"
& "\\10.16.56.6\chocolatey$\_install_etab\Ti\TI-Connect_FR-4.0.0.218.exe"
& "\\10.16.56.6\chocolatey$\_install_etab\Ti\TI-SmartViewTI-83Plus.fr-1.1.0.exe"
pause
echo "--------------------------"
echo "Installation de MindView"
echo "--------------------------"
start notepad '\\10.16.56.6\chocolatey$\_install_etab\Mindview\Mindview.txt'
& "\\10.16.56.6\chocolatey$\_install_etab\Mindview\mindview7_fr_b18668.exe"
pause
echo "--------------------------"
echo "Installation de Filius"
echo "--------------------------"
& "\\10.16.56.6\chocolatey$\_install_etab\filius\Filius-Setup-1.10.4.exe"

echo "--------------------------"
echo "Installation de Iperf as Service"
echo "--------------------------"
& "\\10.16.56.6\chocolatey$\_install_etab\Iperf\iperf_as_service.bat"


echo "--------------------------"
echo "Installation de Iperf as Service"
echo "--------------------------"
& "\\10.16.56.6\chocolatey$\_install_etab\EduPython\EP3.exe"


pause
echo "--------------------------"
echo "Installation de l'antivirus"
echo "--------------------------"
net use \\10.16.56.2\ofcscan\ /user:EDU\opc cpo
& "\\10.16.56.2\ofcscan\autopcc.exe"

pause
echo "--------------------------"
echo "Installation de Fog"
echo "--------------------------"
& "\\10.16.56.6\chocolatey$\_install_etab\fog\cmd.bat"
pause

echo "--------------------------"
echo "Fin des installations "
echo "--------------------------"
pause 
