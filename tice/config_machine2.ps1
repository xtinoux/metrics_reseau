
function create_local_user{
    $Password = "eleve" -AsSecureString
    New-LocalUser -AccountNeverExpires -Description "Compte local élève" -Password $Password 
    $Password = "prof" -AsSecureString
    New-LocalUser -AccountNeverExpires -Description "Compte local prof" -Password $Password 
}

function  jonction_domain{    
    Write-Host "Jonction du domaine active directoy"
    Add-Computer -DomainName edu.local -DomainCredential administrateur@edu.local -OUPath "DC=edu,DC=local"
}

function admin{
    return ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")
}

if (admin){
    create_local_user
    jonction_domain
}
else {
    Write-Host("Veuillez relancer powershell en mode administratif")
}