import random 

Laby = [[("SE"),("SE"),("E"),("S"),("SE"),("E"),("SE")],
[("S"),("SE"),("E"),("SE"),("E"),("N"),("SE")],
[("E"),("E"),("E"),("NE"),("E"),("E"),("SE")]]

class Individu():
    def __init__(self):
        self.pos = [0,0]
        self.genome = [ random.choice(["N","E","S","O"]) for i in range(100)]
        self.etat = True
        self.nb_pas = 0

    def avance(self):
        self.nb_pas += 1
        sens = self.genome.pop(0)
        print(sens)

print("Bonjour")
perso = Individu()
print(perso.genome)
perso.avance()