#######################
#     Third party     #
#######################

choco install dotnetfx- y
choco install powershell- y
choco install jre8- y
choco install adobeair -y
choco install flashplayerplugin -y
choco install flashplayerppapi -y


#######################
#      Base      #
#######################

choco install 7zip -y
choco install vlc -y
choco install firefox -y
choco install chromium -y
choco install libreoffice-still -y
choco install sumatrapdf.install -y
choco install putty.install -y
choco install gimp -y

#######################
#      SNT - NSI      #
#######################

choco install arduino -y
choco install vscode-arduino -y
choco install vscodium -y
choco install python3 -y
choco install vscode-python -y
choco install pyqt5 -y
choco install pyzo -y
choco install numpy -y


#######################
#       PHY-CHI       #
#######################

choco install avogadro -y